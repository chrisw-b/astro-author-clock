import { defineConfig } from "astro/config";
import auth from "auth-astro";

import node from "@astrojs/node";

export default defineConfig({
  site: "https://time.chriswb.dev",
  integrations: [auth()],
  output: "server",
  server: { port: +(process.env.PORT ?? 4321) },
  adapter: node({ mode: "standalone" }),
});
