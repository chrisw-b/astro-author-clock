import Github from "@auth/core/providers/github";
import { defineConfig } from "auth-astro";

export default defineConfig({
  trustHost: true,
  secret: import.meta.env.AUTH_SECRET ?? process.env.AUTH_SECRET ?? "",
  providers: [
    Github({
      clientId:
        import.meta.env.GITHUB_CLIENT_ID ?? process.env.GITHUB_CLIENT_ID ?? "",
      clientSecret:
        import.meta.env.GITHUB_CLIENT_SECRET ??
        process.env.GITHUB_CLIENT_SECRET ??
        "",
    }),
  ],
});
