export type QuoteRow = {
  time_of_day: string;
  time_phrase: string;
  quote: string;
  book: string | null;
  author: string | null;
  approved?: boolean;
  id?: string;
};
