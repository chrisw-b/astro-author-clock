import type { APIRoute } from "astro";
import { add, format, parse } from "date-fns";
import type { QuoteRow } from "~/datamodel";
import sql from "~/pages/api/db";

// Returns the next 10 mins of quotes for a given time_of_day
export const GET: APIRoute = async ({ url }) => {
  const timeOfDay = url.searchParams.get("time_of_day");

  if (!timeOfDay) {
    return new Response(null, {
      status: 400,
      statusText: "No time provided",
    });
  }

  const parsedTime = parse(timeOfDay, "HH:mm", new Date());
  const endTime = add(parsedTime, { minutes: 10 });

  try {
    // connect and query DB for times between timeOfDay and timeOfDay + 10mins
    const times = await sql<QuoteRow[]>`
      select
        TO_CHAR(time_of_day, 'HH24:MI') AS time_of_day,
        time_phrase,
        quote,
        book,
        author
      from quotes
      where
        time_of_day >= ${timeOfDay}
        and time_of_day <= ${format(endTime, "HH:mm")}
        and approved = True
    `;

    return new Response(JSON.stringify(times), {
      status: 200,
      headers: { "Content-Type": "application/json" },
    });
  } catch (_e) {
    return new Response(JSON.stringify([]), {
      status: 500,
      headers: { "Content-Type": "application/json" },
    });
  }
};
