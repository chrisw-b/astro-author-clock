import type { APIRoute } from "astro";
import sql from "~/pages/api/db";

export const POST: APIRoute = async ({ request, redirect }) => {
  const data = await request.formData();

  const approvedIds: string[][] = [];
  const deniedIds: string[] = [];

  data.forEach((approved, id) => {
    if (approved === "true") {
      approvedIds.push([id]);
    } else if (approved === "false") {
      deniedIds.push(id);
    }
  });

  try {
    // connect and send to db
    if (approvedIds.length) {
      await sql`
          update quotes set approved = true
          from (values ${sql(approvedIds)}) as update_data (id)
          where quotes.id = (update_data.id)::int
        `;
    }
    if (deniedIds.length) {
      await sql`
          delete from quotes where id in ${sql(deniedIds)}
        `;
    }

    return redirect("/approve?update=success", 303);
  } catch (e) {
    console.error(e);
    return redirect("/approve?update=error", 303);
  }
};
