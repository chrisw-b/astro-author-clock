import postgres from "postgres";

const sql = postgres("", {
  host: import.meta.env.PGHOST,
  port: +import.meta.env.PGPORT,
  database: import.meta.env.PGDATABASE,
  username: import.meta.env.PGUSER,
  password: import.meta.env.PGPASSWORD,
  ssl: "prefer",
});

export default sql;
