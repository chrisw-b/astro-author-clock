import type { APIRoute } from "astro";
import createDOMPurify from "dompurify";
import { JSDOM } from "jsdom";
import type { QuoteRow } from "~/datamodel";

import sql from "./db";

const window = new JSDOM("").window;
const DOMPurify = createDOMPurify(window);

export const POST: APIRoute = async ({ request, redirect }) => {
  const data = await request.formData();

  const quote = DOMPurify.sanitize(data.get("quote") as string);
  const book = DOMPurify.sanitize(data.get("book") as string);
  const author = DOMPurify.sanitize(data.get("author") as string);
  const time_phrase = DOMPurify.sanitize(data.get("phrase") as string);
  const time_of_day = DOMPurify.sanitize(data.get("time") as string);

  if (!(quote && time_phrase && time_of_day)) {
    return redirect("/submit?update=error", 303);
  }
  try {
    // connect and send to db
    const newQuote: QuoteRow = {
      quote,
      book,
      author,
      time_phrase,
      time_of_day,
      approved: false,
    };
    await sql`
      insert into quotes
        ${sql(newQuote, "quote", "book", "author", "time_of_day", "time_phrase", "approved")}
    `;
    return redirect("/submit?update=success", 303);
  } catch (e) {
    console.error(e);
    return redirect("/submit?update=error", 303);
  }
};
