/// <reference path="../.astro/types.d.ts" />
/// <reference types="astro/client" />
interface ImportMetaEnv {
  readonly PGUSER: string;
  readonly PGPASSWORD: string;
  readonly PGHOST: string;
  readonly PGPORT: string;
  readonly PGDATABASE: string;

  readonly AUTH_SECRET: string;
  readonly GITHUB_CLIENT_ID: string;
  readonly GITHUB_CLIENT_SECRET: string;
}

interface ImportMeta {
  readonly env: ImportMetaEnv;
}
